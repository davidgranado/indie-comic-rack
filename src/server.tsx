// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

import * as functions from 'firebase-functions';
import * as React from 'react';
import { renderToString } from 'react-dom/server';
import * as express from 'express';
import { StaticRouter } from 'react-router-dom';
import * as fs from 'fs';
import { App } from './app';
import { listingsData } from './data';

const app = express();
const index = fs.readFileSync(`${__dirname}/index.html`, 'utf8');

app.get('**', (req, res) => {
	const context = {};
	const html = renderToString(
		<StaticRouter location={req.url} context={context}>
			<App listingData={listingsData}/>
		</StaticRouter>
	);
	res.set('Cache-Control', 'public, max-age=600, s-maxage=1200');
	res.send(
		index
			.replace('<!--APP-->', html)
			.replace('/****LISTING_DATA****/', JSON.stringify(listingsData))
	);
});

export
const appFunctions = functions.https.onRequest(app);
