export
interface Sample {
	label: string;
	url: string;
	type: 'LINK' | 'IFRAME' | 'IMAGE';
}

export
interface Credit {
	name: string;
	credit: string;
	url?: string;
}

export
interface Listing {
	id: string;
	title: string;
	image: string;
	homepage: string;
	samples: Sample[];
	credits: Credit[];
	description: string;
	tags: string[];
}