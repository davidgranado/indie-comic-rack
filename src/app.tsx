import * as React from 'react';
import { Route, Redirect, Switch, Link, withRouter } from 'react-router-dom';
import { Listing } from './models';
import { Foo } from './components/foo';
import { categories } from './categories';
import { Segment, Menu, Container } from 'semantic-ui-react';
import { Newsletter } from './components/newsletter';
import { FixedMenu } from './components/fixed-menu';

const MENU_CATEGORIES = categories.slice(0).sort((a, b) => a.menuOrder - b.menuOrder);

interface Props {
	listingData: Listing[];
}

interface State {
	listingData: Listing[];
}

class TwitterShare extends React.Component {
	componentDidMount() {
		const twttr = (window as any).twttr;

		if(twttr) {
			twttr.widgets.load();
		}
	}

	shouldComponentUpdate() {
		return false;
	}

	render() {
		return (
			<a
				href="https://twitter.com/IndieComicRack"
				className="twitter-follow-button"
				data-size="large"
				data-dnt="true"
				data-show-count="false"
			>
				Follow @IndieComicRack
			</a>
		);
	}
}

const AppHeader = withRouter(({location}) => {
	let activePath = '/';

	if(location.pathname.indexOf('/category/') === 0) {
		activePath = location.pathname.replace('/category', '');
	}
	return (
		<Segment inverted vertical>
			<Container>
				<Menu secondary inverted pointing size="large">
					{MENU_CATEGORIES.map(c => (
						<Link
							key={c.path}
							to={c.path === '/' ? c.path : `/category${c.path}`}>
							<Menu.Item active={c.path === activePath}>
								{c.label}
							</Menu.Item>
						</Link>
					))}
					<div className="right item">
						{/* <a className="ui inverted button">Log in</a>
						<a className="ui inverted button">Sign Up</a> */}
						<TwitterShare />
					</div>
				</Menu>
			</Container>
			<div className="header-logo">
				<img src="/assets/images/icr-full-blue-single-line-800.png" alt="IndieComicRack"/>
				<div>
					Want to be looped in on indie comic happenings?
					<p>
						Subscribe to the newsletter!<br/>
					</p>
					<Newsletter/>
				</div>
			</div>
		</Segment>
	);
});

export
class App extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);
		this.state = {
			listingData: props.listingData,
		};
	}

	render() {
		const {
			listingData,
		} = this.state;

		return (
			<>
				<AppHeader/>
				<FixedMenu/>
				<Switch>
					<Route
						path={`/category/:category`}
						component={({location, match}) => (
							<>
								<Foo
									listingData={
										listingData
											.filter(l => l.tags.includes(match.params.category))
									}
									category={match.params.category}
									rootPath={location.pathname}
									path={location.pathname}
								/>
							</>
						)}
					/>
					<Route
						path="/"
						component={({location}) => <Foo
							listingData={listingData}
							path={location.pathname}
						/>}
					/>
					<Redirect to="/" />
				</Switch>
			</>
		);
	}
}

