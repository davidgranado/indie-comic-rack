import * as React from 'react';
import { Icon } from 'semantic-ui-react';

export
class Newsletter extends React.Component {
	shouldComponentUpdate() {
		return false;
	}

	render() {
		return (
			<>
				<div id="mc_embed_signup">
					<form
						action="https://indiecomicrack.us20.list-manage.com/subscribe/post?u=e2c97cb9da8fcbf82018d0792&amp;id=ba28d018c6"
						method="post"
						id="mc-embedded-subscribe-form"
						name="mc-embedded-subscribe-form"
						className="validate"
						target="_blank"
						noValidate
					>
						<div id="mc_embed_signup_scroll" className="ui action input">
							<input
								type="email"
								defaultValue=""
								name="EMAIL"
								className="email"
								id="mce-EMAIL"
								placeholder="email address"
								required
							/>
							<button
								value="Subscribe"
								name="subscribe"
								id="mc-embedded-subscribe"
								className="ui teal right labeled icon button"
							>
								<Icon name="envelope open outline" />
								Subscribe
							</button>
						</div>
						{/* real people should not fill this in and expect good things - do not remove this or risk form bot signups */}
						<div
							style={{
								position: 'absolute',
								left: -5000
							}}
							aria-hidden="true"
						>
							<input
								type="text"
								name="b_e2c97cb9da8fcbf82018d0792_ba28d018c6"
								tabIndex={-1}
								defaultValue=""
							/>
						</div>
					</form>
				</div>
			</>
		);
	}
}
