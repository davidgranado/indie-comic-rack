import * as React from 'react';
import { Modal, Image, Label, Input, FormField, Icon, Grid, GridColumn, Container, Button, Segment } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import { PreviewCarousel } from './preview-carousel';
import { Listing } from '../models';

interface Props {
	id: string;
	listingData: Listing[];
	rootPath: string;
}

interface State {
	close: boolean;
	showCopied: boolean;
}

export
class ListingModal extends React.Component<Props, State> {
	containerRef: React.Ref<HTMLDivElement>;
	constructor(props: Props) {
		super(props);
		this.state = {
			showCopied: false,
			close: false,
		};
		this.containerRef = React.createRef<HTMLDivElement>();
	}
	componentDidMount() {
		const twttr = (window as any).twttr;

		if(twttr) {
			twttr.widgets.load();
		}

		setTimeout(() => {
			(this.containerRef as any).current.scrollIntoView({
				block: 'center',
			});
		}, 300);
	}

	close() {
		this.setState({
			close: true,
		});
	}

	render() {
		const {
			rootPath,
			id,
		} = this.props;
		const listing = this.props.listingData.find(l => l.id === id);

		if(!listing || this.state.close) {
			return (
				<Redirect to={rootPath} />
			);
		}

		const {
			credits,
			description,
			homepage,
			image,
			samples,
			tags,
			title,
		} = listing;

		return (
			<div className="listing-modal" ref={this.containerRef}>
				<meta name="twitter:card" content="summary" />
				<meta name="twitter:site" content="@indiecomicrack" />
				<meta property="og:url" content={`https://indiecomicrack.com/listing/${id}`} />
				<meta property="og:title" content={`IndieComicRack: ${title}`} />
				<meta property="og:description" content={description.replace(/<\/?[^>]+(>|$)/g, "")} />
				<meta property="og:image" content={`https://indiecomicrack.com/assets/images/listings-main/${image}`}  />
				<Grid stackable columns="16">
						<Grid.Row>
							<Grid.Column width="16" textAlign="right">
								<Button primary onClick={() => this.close()} icon>
									<Icon name="close"/>
								</Button>
							</Grid.Column>
						</Grid.Row>
					<Grid.Row>
						<GridColumn width="6" className="listing-modal-image-col" verticalAlign="middle">
							<Image
								wrapped
								size="medium"
								title={title}
								src={`assets/images/listings-main/${image}`}
							/>
						</GridColumn>
						<GridColumn width="10" textAlign="left" stretched>
							<Grid className="listing-modal-content">
								<Grid.Row>
									<Grid.Column>
										<h2>
											<a target="_blank" href={homepage}>
												{title} <Icon name="external" />
											</a>
										</h2>
										{!!samples.length && (
											<PreviewCarousel
												title={title}
												samples={samples}
											/>
										)}
										<p dangerouslySetInnerHTML={{__html: description}} />
									</Grid.Column>
								</Grid.Row>
								<Grid.Row verticalAlign="bottom">
									<Grid.Column verticalAlign="bottom">
										<p>
											{credits.map((credit, i) => (
												credit.url ? (
													<a
														key={i}
														href={credit.url}
														target="_blank"
														className="ui teal image label large"
													>
														{credit.name}
														<span className="detail">{credit.credit}</span>
													</a>
												) : (
													<span key={i} className="ui teal image label large">
														{credit.name}
														<span className="detail">{credit.credit}</span>
													</span>
												)
											))}
										</p>
										<p>
											{tags.map(tag => (
												<Label
													tag
													key={tag}
													as="span"
													color="green"
													size="large"
												>
													{tag}
												</Label>
											))}
											<a
												href="https://twitter.com/share?ref_src=twsrc%5Etfw"
												className="twitter-share-button"
												data-size="large"
												data-text={`Check out ${title} on @IndieComicRack`}
												data-hashtags="indiecomics #comicbooks #teamcomics"
												data-url={`https://indiecomicrack.com/listing/${id}`}
												data-dnt="true"
											>
												Tweet
											</a>
										</p>
									</Grid.Column>
								</Grid.Row>
							</Grid>
						</GridColumn>
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}