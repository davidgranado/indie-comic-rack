import * as React from 'react';
import { Grid, Image, Header } from 'semantic-ui-react';
import { Listing } from '../models';
import { Link } from 'react-router-dom';
import { LazyLoadProps } from 'react-lazyload';
// import LazyLoad from 'react-lazyload';

// See if I can work out scrolling wonk with lazy load.
const LazyLoad = ({children}: LazyLoadProps) => <>{children}</>;
// Above fragment notation used to work around this issue:
// https://github.com/DefinitelyTyped/DefinitelyTyped/issues/18051

interface Props extends Listing {
	hidden?: boolean;
	rootPath?: string;
	onTagSelect(category: string): void;
}

export
class ListingComponent extends React.Component<Props> {
	render() {
		const {
			id,
			hidden,
			title,
			image,
			rootPath,
		} = this.props;
		return (
			<Grid.Column className={`listing ${hidden ? 'hidden' : ''}`}>
				<Link
					to={`${rootPath}listing/${id}`}
					className="four wide computer four wide tablet six wide mobile column"
				>
					<div className="listing-title-text">
						{title}
					</div>
					<LazyLoad once offset={100}>
						<Image
							centered
							className="listing-cover shadow"
							src={`assets/images/listings-main/${image}`}
						/>
					</LazyLoad>
				</Link>
			</Grid.Column>
		);
	}
}