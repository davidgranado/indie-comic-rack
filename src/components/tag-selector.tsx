import * as React from 'react';
import { Dropdown } from 'semantic-ui-react';

interface Props {
	selectedTag: string;
	tags: string[];
	onTagSelect(tag: string): void;
}

function handleKeyUp(value: string, keyCode: number, selectHandler) {
	if(keyCode === 8 && !value) {
		selectHandler('all');
	}
}

export
function TagSelector(props: Props) {
	const {
		selectedTag,
		tags,
		onTagSelect,
	} = props;
	return (
		<Dropdown
			fluid
			search
			selection
			size="massive"
			className="massive"
			placeholder="Select Country"
			value={selectedTag}
			onKeyUp={(e) => handleKeyUp(e.target.value, e.keyCode, onTagSelect)}
			options={[{
				key: 'all',
				text: 'all',
				value: 'all',
			}].concat(
				tags.map(tag => ({
					key: tag,
					text: tag,
					value: tag,
				}))
			)}
			onChange={(e, input) => onTagSelect(input.value as string)}
		/>
	);
}
