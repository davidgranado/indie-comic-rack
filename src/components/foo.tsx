import * as React from 'react';
import { Grid, Container, Input, Menu, Header, Segment, Button } from 'semantic-ui-react';
import { TagSelector } from '../components/tag-selector';
import { ListingComponent } from '../components/listing';
import { forceCheck } from 'react-lazyload';
import { debounce } from '../utils/debounce';
import { ListingModal } from '../components/listing-modal';
import { Route, Redirect, Switch, Link } from 'react-router-dom';
import { Listing } from '../models';
import { shuffleArray } from '../utils';

interface Props {
	category?: string;
	listingData: Listing[];
	path: string;
	rootPath?: string;
}

interface State {
	listingData: Listing[];
	selectedTag: string;
	searchString: string;
	showMap: boolean[];
}

interface ListingShowMap {
	listing: Listing;
	show: boolean;
}

function listToRowCols(list: Listing[], showMap: boolean[]) {
	const data: ListingShowMap[][] = [];

	list
		.forEach((listing, i) => {
		const row = Math.floor(i/4);
		const col = i%4;
		if(!col) {
			data[row] = [];
		}
		data[row].push({
			show: showMap[i],
			listing,
		});
	});

	return data;
}

export
class Foo extends React.Component<Props, State> {
	Tags: string[];

	constructor(props: Props) {
		super(props);
		this.state = {
			listingData: props.listingData,
			selectedTag: 'all',
			searchString: '',
			// Initialize all to "show"
			showMap: props.listingData.map(() => true),
		};

		this.Tags = Object.keys(
			props.listingData.reduce((tags, listing) => {
				listing.tags.forEach(t => tags[t] = true);
				return tags;
			}, {})
		).sort();
	}

	updateTag(selectedTag: string) {
		this.setState({selectedTag}, () => this.updateShowMap());
	}

	updateSearch(searchString: string) {
		this.setState({searchString}, () => this.updateShowMap());
	}

	handleTagClick(selectedTag: string) {
		this.updateTag(selectedTag);
		scrollTo(0, 0);
	}

	shuffleList() {
		this.setState({
			listingData: shuffleArray(this.state.listingData),
		}, () => forceCheck());
	}

	updateShowMap = debounce(() => {
		const {
			listingData,
			selectedTag,
		} = this.state;
		const searchString = this.state.searchString.toLocaleLowerCase();

		const showMap = listingData
			.map(function(l) {
				if(selectedTag !== 'all') {
					if(!l.tags.some((t) =>
						t.toLocaleLowerCase() === selectedTag
					)) {
						return false;
					}
				}

				if(!searchString) {
					return true;
				}

				return l.title.toLocaleLowerCase().includes(searchString) ||
					l.description.toLocaleLowerCase().includes(searchString) ||
					l.credits.some(function(c) {
						return c.name.toLocaleLowerCase().includes(searchString);
					}) ||
					l.tags.some(function(t) {
						return t.toLocaleLowerCase().includes(searchString);
					});
			});

		this.setState({
			showMap,
		}, () => {
			setTimeout(() => forceCheck(), 400);
		});
	}, 300);

	render() {
		const {
			listingData,
			searchString,
			selectedTag,
			showMap,
		} = this.state;
		const {
			category = '',
			path,
		} = this.props;
		const rowCols = listToRowCols(listingData, showMap);

		const rootPath = category ? `/category/${category}/` : '/';

		return (
			<>
				<Container className="main-container">
					<Grid stackable columns="16">
						<Grid.Column width="11">
							<Input
								fluid
								icon="search"
								placeholder="Search title, tag, creator, etc" size="massive"
								value={searchString}
								onChange={({target}) => this.updateSearch((target as any).value)}
							/>
						</Grid.Column>
						<Grid.Column width="5">
							<TagSelector
								selectedTag={selectedTag}
								tags={this.Tags}
								onTagSelect={(tag) => this.updateTag(tag)}
							/>
						</Grid.Column>
					</Grid>
					<Button
						fluid
						primary
						className="listing-shuffle-button"
						size="massive"
						onClick={() => this.shuffleList()}
					>
						Shuffle Rack!
					</Button>
					<Container className="listing-container">
						<Grid centered verticalAlign="bottom" stackable columns="4">
							{rowCols.map((row, r) => (
								<React.Fragment key={r}>
									<Grid.Row>
										{row.map(({listing, show}, c) => (
											<ListingComponent
												key={listing.id}
												hidden={!show}
												rootPath={rootPath}
												{...listing}
												onTagSelect={(tag) => this.handleTagClick(tag)}
											/>
										))}
									</Grid.Row>
									<Grid.Row>
										{row.map(({listing}, c) => (
											<Route
												key={`preview-${listing.id}`}
												path={`${rootPath}listing/${listing.id}`}
												component={() => (
													<ListingModal
														id={listing.id}
														listingData={listingData}
														rootPath={rootPath}
													/>
												)}
											/>
										))}
									</Grid.Row>
								</React.Fragment>
							))}
						</Grid>
					</Container>
				</Container>
				{path === rootPath && (
					<>
						<meta name="twitter:card" content="summary" />
						<meta name="twitter:site" content="@indiecomicrack" />
						<meta property="og:url" content="https://indiecomicrack.com/" />
						<meta property="og:title" content="IndieComicRack: Browse Indie Comics" />
						<meta property="og:description" content="A site meant to promote smaller, independent comics. Browse a collection of previews and learn about the creators by discovering their homepages and storefronts." />
						<meta property="og:image" content="https://indiecomicrack.com/assets/images/logo-twitter.png" />
					</>
				)}
			</>
		);
	}
}

