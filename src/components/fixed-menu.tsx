import * as React from 'react';
import { Container, Menu, Visibility, Transition } from 'semantic-ui-react';
import { categories } from '../categories';
import { Newsletter } from './newsletter';

const MENU_CATEGORIES = categories.sort((a, b) => a.menuOrder - b.menuOrder);

interface State {
	visible: boolean;
}

export
class FixedMenu extends React.Component<{}, State> {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
		};
	}
	render() {
		return (
			<Visibility
				continuous
				fireOnMount
				onBottomPassed={() => this.setState({visible: true})}
				onBottomPassedReverse={() => this.setState({visible: false})}
			>
				<Transition
					visible={this.state.visible}
					duration={500}
				>
					<Menu size="large" fixed="top">
						<Container>
							<div/>
							<Menu.Item position="right">
								<Newsletter/>
							</Menu.Item>
						</Container>
					</Menu>
				</Transition>
			</Visibility>
		);
	}
}
