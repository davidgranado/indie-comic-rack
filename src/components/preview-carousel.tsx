import * as React from 'react';
import { Modal, Header, Button, Icon, Image, Container } from 'semantic-ui-react';
import { Sample } from '../models';

interface Props {
	title: string;
	samples: Sample[];
}

interface State {
	currentIndex: number;
}

export
class PreviewCarousel extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);
		this.state = {
			currentIndex: 0,
		};
	}

	next() {
		let nextIndex = this.state.currentIndex + 1;

		if(nextIndex >= this.props.samples.length) {
			nextIndex = 0;
		}

		this.setState({
			currentIndex: nextIndex,
		})
	}

	prev() {
		let nextIndex = this.state.currentIndex - 1;

		if(nextIndex < 0) {
			nextIndex = this.props.samples.length - 1;
		}
		this.setState({
			currentIndex: nextIndex,
		})
	}

	render() {
		const {
			samples,
			title,
		} = this.props;
		const {
			currentIndex,
		} = this.state;
		const currentSample = samples[currentIndex];

		return (
			<Modal size="large" className="preview-carousel" closeIcon trigger={<div className="previews-trigger">Previews</div>}>
				<Header content={`Preview: ${title}`} />
				<Modal.Content>
					<Modal.Actions as="p">
						{currentIndex + 1} / {samples.length}
						<Button color="green" inverted  onClick={() => this.prev()}>
							<Icon  name="arrow left"/> Prev
						</Button>
						<Button color="green" inverted onClick={() => this.next()}>
							<Icon name="arrow right" /> Next
						</Button>
					</Modal.Actions>
					<Container as="p" textAlign="center">
						{currentSample.type === 'LINK' && (
							<a target="_blank" href={currentSample.url}>
								To Preview <Icon name="external" />
							</a>
						)}
						{currentSample.type === 'IFRAME' && (
							<iframe className="preview-iframe-window" src={currentSample.url}></iframe>
						)}
						{currentSample.type === 'IMAGE' && (
							<Image
								target="_blank"
								size="medium"
								src={currentSample.url}
								href={currentSample.url}
							/>
						)}
					</Container>
				</Modal.Content>
			</Modal>
		);
	}
}