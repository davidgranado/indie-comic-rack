export
function debounce(func, wait, immediate = false) {
	let timeout, args, context, timestamp, result;
	// tslint:disable-next-line:no-parameter-reassignment
	if (null === wait) wait = 100;
  
	function later() {
	  const last = Date.now() - timestamp;
  
	  if (last < wait && last >= 0) {
		timeout = setTimeout(later, wait - last);
	  } else {
		timeout = null;
		if (!immediate) {
		  result = func.apply(context, args);
		  context = args = null;
		}
	  }
	};
  
	const debounced = function(){
	  // tslint:disable-next-line:no-invalid-this
	  context = this;
	  args = arguments;
	  timestamp = Date.now();
	  const callNow = immediate && !timeout;
	  if (!timeout) timeout = setTimeout(later, wait);
	  if (callNow) {
		result = func.apply(context, args);
		context = args = null;
	  }
  
	  return result;
	};
  
	(debounced as any).clear = function() {
	  if (timeout) {
		clearTimeout(timeout);
		timeout = null;
	  }
	};
	
	(debounced as any).flush = function() {
	  if (timeout) {
		result = func.apply(context, args);
		context = args = null;
		
		clearTimeout(timeout);
		timeout = null;
	  }
	};
  
	return debounced;
}