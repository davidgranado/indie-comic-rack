// ordered by route in list. Menu order is arbitrary
export
const categories = [
	{
		label: 'Action',
		tag: 'action',
		path: '/action/',
		menuOrder: 1,
	},
	{
		label: 'Sci-Fi',
		tag: 'sci-fi',
		path: '/sci-fi/',
		menuOrder: 2,
	},
	{
		label: 'Horror',
		tag: 'horror',
		path: '/horror/',
		menuOrder: 3,
	},
	{
		label: 'Home',
		tag: 'all',
		path: '/',
		menuOrder: 0,
	},
]