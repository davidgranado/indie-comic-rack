import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './app';
import { BrowserRouter } from 'react-router-dom';

const listingData = (window as any).browserListingData;

ReactDOM.render(
	<BrowserRouter basename="/">
		<App listingData={listingData}/>
	</BrowserRouter>,
	document.getElementById('app'),
);