function debounce(func, wait, immediate){
	var timeout, args, context, timestamp, result;
	if (null == wait) wait = 100;
  
	function later() {
	  var last = Date.now() - timestamp;
  
	  if (last < wait && last >= 0) {
		timeout = setTimeout(later, wait - last);
	  } else {
		timeout = null;
		if (!immediate) {
		  result = func.apply(context, args);
		  context = args = null;
		}
	  }
	};
  
	var debounced = function(){
	  context = this;
	  args = arguments;
	  timestamp = Date.now();
	  var callNow = immediate && !timeout;
	  if (!timeout) timeout = setTimeout(later, wait);
	  if (callNow) {
		result = func.apply(context, args);
		context = args = null;
	  }
  
	  return result;
	};
  
	debounced.clear = function() {
	  if (timeout) {
		clearTimeout(timeout);
		timeout = null;
	  }
	};
	
	debounced.flush = function() {
	  if (timeout) {
		result = func.apply(context, args);
		context = args = null;
		
		clearTimeout(timeout);
		timeout = null;
	  }
	};
  
	return debounced;
};

$(document)
.ready(function() {
	// fix menu when passed
	$('.masthead')
		.visibility({
			once: false,
			onBottomPassed() {
				$('.fixed.menu').transition('fade in');
			},
			onBottomPassedReverse() {
				$('.fixed.menu').transition('fade out');
			},
		});
	$('.ui.sidebar')
		.sidebar('attach events', '.toc.item');
	$('.ui.dropdown')
		.dropdown();


	let selectedCategory = 'all';
	let searchString = '';

	(function() {
		document.querySelectorAll('.listing-category').forEach(el => 
			el.addEventListener('click', (e) => {
				$('.category-search').dropdown('set selected', e.target.innerText);
				scrollTo(0, 0);
			})
		);
	})();

	(function() {
		// Get tag list
		const tagMap = listingsData.reduce((categories, listing) => {
			listing.tag.forEach(t => categories[t] = true);
			return categories;
		}, {});
		const tags = Object.keys(tagMap).sort();

		// console.log(tags.map(t => `<option value="${t}" select="selected">${t}</option>`));

		document.querySelector('.category-search')
			.addEventListener('change', (e) => {
				const el = e.target;
				selectedCategory = el.options[el.selectedIndex].value;
				searchListings(searchString, selectedCategory);
			});
		document.querySelector('.category-search')
			.addEventListener('keyup', (e) => {
				const el = e.target;
				if(!e.target.value && [8, 27].includes(e.keyCode)) {
					$('.category-search').dropdown('set selected', 'all');
				}
			});
	})();

	(function() {
		// Handle search
		document.querySelector('.listing-search')
			.addEventListener('input', debounce(function(e) {
				searchString = e.target.value.toLocaleLowerCase();
				searchListings(searchString, selectedCategory);
			}, 300));
	})();

	function searchListings(searchString, selectedCategory) {
		const showMap = listingsData
			.map(function(l) {
				if(selectedCategory !== 'all') {
					if(!l.tag.some((t) => 
						t.toLocaleLowerCase() === selectedCategory
					)) {
						return false;
					}
				}

				if(!searchString) {
					return true;
				}

				return l.title.toLocaleLowerCase().includes(searchString) ||
					l.description.toLocaleLowerCase().includes(searchString) ||
					l.credits.some(function(c) {
						return c.name.toLocaleLowerCase().includes(searchString);
					}) ||
					l.tag.some(function(t) {
						return t.toLocaleLowerCase().includes(searchString);
					});
			});

		const listingEls = document.querySelectorAll('.listing');

		showMap.map((show, i) => {
			if(show) {
				listingEls[i].classList.remove('hidden');
			} else {
				listingEls[i].classList.add('hidden');
			}
		});
	}
});

const listingsData = [{
	title: 'Karma',
	image: 'assets/images/listings-main/karma.jpg',
	homepage: 'http://mindywheelerart.com/',
	samples: [],
	credits: [{
		name: 'Mindy Wheeler',
		credit: 'Creator',
		url: 'http://mindywheelerart.com/'
	}],
	description: `You call it "Limbo", I call it The Etherworld - where the Damned are itching to break free. When the very threads of life & death unravel, it becomes one twisted, ugly fight to control the power of the Etherverse. The most wicked of all have been plotting their take-over for centuries - who will finally reign?

	Escape in this adventurous mythological fantasy about the fabric of morality, insatiable appetites, and the webs of wonder in an ancient world. It all begins with one brash and willful girl named Karma.`,
	tag: [
		'adventure',
		'comicsgate',
		'fantasy',
		'mythology'
	],
}, {
	title: 'T-Bird & Throttle',
	image: 'assets/images/listings-main/sway.jpg',
	homepage: 'https://www.indiegogo.com/projects/t-bird-throttle-a-new-comic-by-josh-howard',
	samples: [{
		label: 'Issue 0 (complete)',
		url: 'https://drive.google.com/file/d/1Vzua5IIKC3iVKdsnYB4uhzfK39HOBLac/view?usp=sharing',
	}, {
		label: 'Issue 1 (first 30 pages)',
		url: 'https://drive.google.com/file/d/1JQzCBe6_5ADqyfnGLICz2XKVczPoTi4L/view?usp=sharing'
	}],
	credits: [{
		name: 'Josh Howard',
		credit: 'Writer/Artist',
	}],
	description: 'T-BIRD & THROTTLE is my passion project, inspired by the comics I grew up reading in the 80s & 90s. The story follows Mitchell Maddox, astronaut turned superhero who was granted incredible powers by a mysterious alien "engine." As T-Bird, Maddox rose to fame as the defender of Centennial City - until the day a terrible tragedy stripped him of everything. 10 yrs. later, Maddox, now a single father, sets out to revive his glory days as T-Bird -but the demons from his past may have other plans...',
	tag: [
		'superhero',
		'sci-fi',
	],
}, {
	title: 'SWAY: REIGN',
	image: 'assets/images/listings-main/sway.jpg',
	homepage: 'https://www.indiegogo.com/projects/sway-reign/x/19378705',
	samples: [{
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1539406563/e33tx3n45mulzmlzzjkt.jpg',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1539406661/llq2vg1ddgcs1xcwsmgc.jpg',
	}, {
		label: 'Page 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1539406878/cvzfo4miv2qiaobkrqgh.jpg',
	}, {
		label: 'Page 4',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1539406921/rd1qdheyvhrrz2r2knoe.jpg',
	}, {
		label: 'Page 5',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1539406959/q7vw8ggwqu780fjmfgr2.jpg',
	}],
	credits: [{
		name: 'Joseph White',
		credit: 'Creator',
	}, {
		name: 'Liam Gray',
		credit: 'Creator',
	}],
	description: `"What if one day you woke up only to discover everything you ever knew was a lie?"

	After a terrible accident, a Postal Worker assigned to Port Lockroy, Antarctica is frozen alive only to be awakened in the distant future by a desperate scientist. Lost and stranded, she must now rely on her wits and every tool at her disposable to survive against mankind’s most ancient adversary!`,
	tag: [
		'sci-fi',
		'horror',
		'comicsgate',
	],
}];
