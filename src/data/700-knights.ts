import { Listing } from "../models";

export
const knights700: Listing = {
	id: '700-knights',
	title: '700 Knights',
	image: '700-knights.jpg',
	homepage: 'https://www.amazon.com/700-Knights-Graphic-John-Rap/dp/194558212X/?utm_source=indiecomicrack',
	description: /*html*/`
		<p>
			In a most brutal and gory battle 700 Knights of St. John defend Europe, at Malta, from a massive Ottoman Empire invasion force. Based on the 1565 Great Siege of Malta. 
		</p>
		<p>
			This graphic novel, collects the four issues in the 700 Knights comic book series.
		</p>
	`,
	samples: [],
	credits: [{
		credit: 'Author',
		name: 'John Rap',
		url: 'https://twitter.com/Johnrap',
	}, {
		credit: 'Illustrator',
		name: 'Lou Manna',
	}],
	tags: [
		'history',
	],
};
