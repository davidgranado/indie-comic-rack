import { Listing } from "../models";

export
const whatIsMyrromGalaxyOath: Listing = {
	id: 'what-is-myrrom-galaxy-oath',
	title: 'Myrrom Galaxy: Oath',
	image: 'what-is-myrrom-galaxy-oath.jpg',
	homepage: 'https://www.ashcancomicspub.com/?utm_source=indiecomicrack',
	description: `A mysterious substance called myrrom arrives on Earth and alters everything it contacts. Dwaine Oakson encounters myrrom and becomes Earth’s mightiest defender, Oath. Wielding super powers and a new heroic mantle, Oath discovers an epic battle for Earth’s future ensuing between two ancient spirits: Peace and Chaos. Siding with Peace, Oath pits himself against champions of Chaos and the manifestations of Chaos itself.`,
	samples: [{
		label: 'Page 1',
		url: 'https://www.ashcancomicspub.com/uploads/2/4/2/5/24254619/myrromgalaxyoath-01-digitaldownload-lindley-14-14_orig.jpeg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://www.ashcancomicspub.com/uploads/2/4/2/5/24254619/myrromgalaxyoath-01-digitaldownload-lindley-17-17_orig.jpeg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://www.ashcancomicspub.com/uploads/2/4/2/5/24254619/myrromgalaxyoath-01-digitaldownload-lindley-19-19_orig.jpeg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Nate Lindley',
		credit: 'Creator',
		url: 'https://twitter.com/LindleyART',
	}],
	tags: [
		'action',
		'sci-fi',
		'superhero',
	],
}