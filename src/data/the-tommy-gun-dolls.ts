import { Listing } from "../models";

export
const theTommyGunDolls: Listing = {
	id: 'the-tommy-gun-dolls',
	title: 'The Tommy Gun Dolls',
	image: 'the-tommy-gun-dolls.jpg',
	homepage: 'https://www.indiegogo.com/projects/the-tommy-gun-dolls-vol-2-graphic-novel?utm_source=indiecomicrack',
	description: `
		<p>
			Frankie, the grifter is back along with those bawdy burlesque girls--Poppy, Mai and Rose as they'll discover what friendship and trust really means to one another. The stakes get higher as they close in on finding Rouletta's murderer while in the midst of an all out turf war between the crime syndicates in San Francisco’s seedy underworld backstreets during the Prohibition Era.
		</p>
		<p>
			Inspired by a true story of San Francisco’s dark underworld, a group of Prohibition Era burlesque dancers pursue their friend’s murder by posing as masked bandits and knocking over the Mob’s speakeasies. The Tommy Gun Dolls is a sordid tale told in the classic Noir tradition, with roots in San Francisco’s historic Nob Hill Mansions of society’s elite class down to the seedy gang-filled streets of Chinatown, through the Tenderloin and neighboring North Beach.
		</p>
	`,
	samples: [{
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1536421976/sxmzp5lowvytmoju0vsk.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1536469979/wjr28zuylonahygpvsnd.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1536422730/x0fzjzwdeelwhfgzgwvb.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1536422746/j0j8itsydgbw7uhdfqbx.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 5',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540086130/fvx59hyep4xbctyer6pp.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 6',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540086491/t6b8yrfskccksetxrp5b.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 7',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540651673/k1ziuervmp0ezndilluw.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 8',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540651762/avo9nwyf2uaekh7fht2p.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 9',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540651824/brakv9zvhmvl7fvof6sq.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 10',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540651890/oj6yxepssicgbht2uhi7.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 11',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540651944/kts1jnij1pkx3zyjavtl.jpg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Dan Cooney',
		credit: 'Writer/Artist',
		url: 'https://www.dancooneyart.com/',
	}],
	tags: [
		'crime',
		'noir',
	],
}