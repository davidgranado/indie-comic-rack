import { Listing } from "../models";

export
const electromagnate: Listing = {
	id: 'electromagnate',
	title: 'Electromagnate: The Book of Rebel Nations',
	image: 'electromagnate.jpg',
	homepage: 'https://www.amazon.com/Electromagnate-Rebel-Nations-John-Rap-ebook/dp/B01B0PU01M/?utm_source=indiecomicrack',
	description: /*html*/`
		<p>
			A dark cabal has usurped the designs of Nikola Tesla's and built a global relay grid of electromagnetic towers. The structured data streams of this new matrix are as ubiquitous as oxygen.
		</p>
		<p>
			Before the conspirators can make everyone's reliance on the data they control as vital as the biological need for oxygen, a final piece is needed. The relay circuit must be closed for the illusions of the data blanket to be complete.
		</p>
		<p>	
			A single tower, on the tiny island nation of Malta, in the center of the Mediterranean Sea, is the last obstacle preventing the villains from deceiving the entire world.
		</p>
	`,
	samples: [],
	credits: [{
		credit: 'Author',
		name: 'John Rap',
		url: 'https://twitter.com/Johnrap',
	}, {
		credit: 'Illustrator',
		name: 'Kostas Pantoulas',
	}, {
		credit: 'Illustrator',
		name: 'N.R. Bharathae',
	}, {
		credit: 'Illustrator',
		name: 'Santosh P. Pillewar',
	}],
	tags: [
		'sci-fi',
		'mythology',
	],
};
