import { Listing } from "../models";

export
const theAbductables: Listing = {
	id: 'the-abductables',
	title: 'The Abductables',
	image: 'the-abductables.jpg',
	homepage: 'https://www.indiegogo.com/projects/the-abductables/?utm_source=indiecomicrack',
	description: /*html*/`
		<p>
			The Abductables is a sci-fi action comedy about an alien abduction gone horribly wrong—for the aliens! Follow a musclebound amnesiac as he battles his way through a flying saucer full of raygun-weilding Greys, mutated monstrosities, and bloodthirsty science experiments in order to find out the truth behind his abduction!
		</p>
	`,
	samples: [],
	credits: [{
		credit: 'Writer',
		name: 'Michael Derrick',
		url: 'https://twitter.com/cauldroncomics',
	}, {
		credit: 'Art',
		name: 'Ibai Canales',
		url: 'https://www.youtube.com/user/Canalus',
	}],
	tags: [
		'action',
		'sci-fi',
	],
}