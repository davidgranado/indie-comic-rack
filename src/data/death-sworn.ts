import { Listing } from "../models";

export
const deathSworn: Listing = {
	id: 'death-sworn',
	title: 'Death Sworn',
	image: 'death-sworn.jpg',
	homepage: 'https://www.etsy.com/uk/listing/569157537/death-sworn-chapter-1/?utm_source=indiecomicrack',
	description: /*html*/`
		<p>
			It is an 80s inspired Space Action Adventure Romp and beautifully illustrated.
		</p>
		<p>
			Death Sworn is about an outlaw (Death Sworn) captured by the evil Darkstar empire who must battle his way through the cosmos while on the run from bounty hunters, pirates and assassins!
		</p>
		<p>
			But what is it he seeks on his quest? Redemption or simply an end to it all!
		</p>
	`,
	samples: [],
	credits: [{
		credit: 'Writer/Illustrator',
		name: 'Karl O\'Rowe',
		url: 'https://twitter.com/Karlorowe',
	}],
	tags: [
		'action',
		'adventure',
		'sci-fi',
		'retro',
	],
}