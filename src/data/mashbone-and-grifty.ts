import { Listing } from "../models";

export
const mashboneAndGrifty: Listing = {
	id: 'mashbone-and-grifty',
	title: 'Mashbone & Grifty',
	image: 'mashbone-and-grifty.jpg',
	homepage: 'https://5meatscomics.bigcartel.com/product/mashbone?utm_source=indiecomicrack',
	description: `
		<p>
			Mexican Crime Lords! La Llorona! Vengeful Ex's!
		</p>
		<p>
			THE CITY NEEDS HEROES! Unfortunately, all it has is a cowardly sad-sack and a simple-minded monkey-man. MASHBONE & GRIFTY, inspired by 80's Action flicks, take to the streets to fight crime and solve mysteries the only way a pair of untrained, unlicensed Private Detectives can: BADLY!
		</p>
	`,
	samples: [],
	credits: [{
		name: 'Oscar Garza',
		credit: 'Art and Stroy',
		url: 'https://twitter.com/5meats',
	}, {
		name: 'Rolando Esquivel',
		credit: 'Edit and Story',
		url: 'https://twitter.com/Rolo5Meats/',
	}],
	tags: [
		'comedy',
		'crime',
	],
}