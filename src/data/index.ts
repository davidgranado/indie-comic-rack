import { Listing } from "../models";
import { theTommyGunDolls } from './the-tommy-gun-dolls';
import { detectiveDeadCrimsonMoon } from "./detective-dead-crimson-moon";
import { whatIsMyrromGalaxyOath } from "./what-is-myrrom-galaxy-oath";
import { mashboneAndGrifty } from "./mashbone-and-grifty";
import { lemonPepperHuggz } from "./lemon-pepper-huggz";
import { electromagnate } from "./electromagnate";
import { knights700 } from "./700-knights";
import { theAbductables } from "./the-abductables";
import { deathSworn } from "./death-sworn";

const listingsData: Listing[] = [{
	id: 'wulf-and-batsy',
	title: 'Wulf and Batsy',
	image: 'wulf-and-batsy.jpg',
	homepage: 'http://www.indyplanet.us/wulf-and-batsy-vol-1/?utm_source=indiecomicrack',
	description: `Wulf and Batsy wander the earth in search of a place to call home. Along their way they run into angry villagers, zombies, mad scientists, and a horde of other weird monsters.`,
	samples: [],
	credits: [{
		name: 'Bryan Baugh',
		credit: 'Creator',
		url: 'http://cryptlogic.net/',
	}],
	tags: [
		'horror'
	],
}, {
	id: 'lady-alchemy',
	title: 'LADY ALCHEMY',
	image: 'lady-alchemy.jpg',
	homepage: 'https://www.indiegogo.com/projects/lady-alchemy-graphic-novel?utm_source=indiecomicrack',
	description: `LADY ALCHEMY is the Burlesque alter ego of Martina Markota, a rising star in the underground arts scene in the big city. In the bustling metropolis of NYC, she discovers that her visions of a demonic world are real. She must control her powers, enter the dark realm and fight to free the city from a mind controlling media executive. Neon Noir meets cyberpunk wrapped around a hot babe in this illustrated action thriller.`,
	samples: [{
		label: 'Art',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1532650947/x1zlad8ltkisdddebahi.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1532651162/e9fcd6ox8q7inatqkhnt.png',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1532651374/eyno2l4mzxhr0vgsreqb.png',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Martina Markota',
		credit: 'Creator',
		url: 'https://www.martinamarkota.com/',
	}, {
		name: 'MG & CS/MAGNUM OPUS studio Productions',
		credit: 'Creator',
	}],
	tags: [
		'horror'
	],
}, {
	id: 'battle-maiden-knuckle-bomb',
	title: 'Battle Maiden Knuckle Bomb',
	image: 'battle-maiden-knuckle-bomb.jpg',
	homepage: 'https://www.indiegogo.com/projects/battle-maiden-knuckle-bomb--2?utm_source=indiecomicrack',
	description: `
	Forced to create genetically modified bioweapons for an archaic occult organization, Dr. Albert Vogel inadvertently succeeds at discovering the means to unlock the limits of human potential. He attempts to conceal his revelation but ends up arousing the suspicion of his oppressors, who now believe the doctor is withholding that which they desperately desire.<br/>
<br/>
Saaya Volkova, Vogel’s niece, is forcefully abducted and used as leverage. She is critically injured and Vogel has no choice but to inject his niece with the perfected formula to save her life. The mutagen transforms Saaya’s body, increasing her skeletal and muscular density, granting her incredible strength and resilience. She also gains the ability to fly and a unique power to absorb and manipulate biokinetic concussive force!<br/>
<br/>
Before she can fully adapt to her body and her newly acquired power, Saaya fights back in a desperate attempt to escape from the clutches of her captors. But even if she and her uncle manage to survive, how long will it take for the agents of chaos to track them down, and just how far does this organization’s influence extend?
	`,
	samples: [{
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1538882658/nsoz5dfxgirkxsc7shcj.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1538882673/fnlzisx3pnuh58pegtgh.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1538882686/t5zyhx4q1x1s89elwzs1.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1538882700/excxwbc9pdn7txro14yo.jpg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Keung Lee',
		credit: 'Creator',
	}],
	tags: [
		'superhero',
		'anime',
		'manga',
		'retro',
	],
}, {
	id: 'the-maroon',
	title: 'The Maroon',
	image: 'the-maroon.png',
	homepage: 'https://www.themarooncomic.com/?utm_source=indiecomicrack',
	samples: [{
		label: 'Page 1',
		url: 'https://static.wixstatic.com/media/c22496_2183fe019b6b4f0fa7c743636786383e~mv2_d_2100_3150_s_2.jpg/v1/fill/w_909,h_1364,al_c,q_90,usm_0.66_1.00_0.01/c22496_2183fe019b6b4f0fa7c743636786383e~mv2_d_2100_3150_s_2.webp',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://static.wixstatic.com/media/c22496_a21187271a184bda8b1914a6437a8651~mv2_d_2100_3150_s_2.jpg/v1/fill/w_909,h_1364,al_c,q_90,usm_0.66_1.00_0.01/c22496_a21187271a184bda8b1914a6437a8651~mv2_d_2100_3150_s_2.webp',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://static.wixstatic.com/media/c22496_2851b74972c44613b00bca1e9858a8b5~mv2_d_2100_3150_s_2.jpg/v1/fill/w_909,h_1364,al_c,q_90,usm_0.66_1.00_0.01/c22496_2851b74972c44613b00bca1e9858a8b5~mv2_d_2100_3150_s_2.webp',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://static.wixstatic.com/media/c22496_9b51ce4184604a07bbdbd62353903b74~mv2_d_2100_3150_s_2.jpg/v1/fill/w_909,h_1364,al_c,q_90,usm_0.66_1.00_0.01/c22496_9b51ce4184604a07bbdbd62353903b74~mv2_d_2100_3150_s_2.webp',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Derek W. Lipscomb',
		credit: 'Writer/Illustrator',
		url: 'http://dereklipscomb4.wixsite.com/dereklipscomb-artist',
	}],
	description: `
	<p>The Maroon is an ongoing comic book series created by Derek W. Lipscomb. The story details the events of a lone nameless Black Seminole, on the run from the law for an atrocity that was pinned on him! But as he searches for sanctuary, he not only encounters fiendish men and women, but mythical figures as well as the supernatural!</p>
	<p>The Maroon is in the tradition of classic American Folklore, but with a modern mature spin.</p>
	<p>It is American Folklore remixed!</p>
`,
	tags: [
		'history',
		'mythology',
	],
}, {
	id: 'hollowed',
	title: 'Hollowed',
	image: 'hollowed.png',
	homepage: 'https://www.comixcentral.com/products/hollowed-issue-one/?utm_source=indiecomicrack',
	description: `Hollowed is a Sci-Fi Horror Comedy that follows the stories of Detectives Vasquez and Tyler as they hunt down a brutal serial killer that hollows out the bodies of its victims. You’ll follow our detectives across states to track down this diabolical deviant with some help that they find along the way. The world of Hollowed is atmospheric, gritty, and oftentimes a lot more darkly comedic than you might expect.`,
	samples: [{
		label: 'Page 1',
		url: '/assets/images/pages/hollowed/page1.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: '/assets/images/pages/hollowed/page2.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: '/assets/images/pages/hollowed/page3.jpg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Casey Bowker',
		credit: 'Creator',
		url: 'https://dontforgetatowel.com'
	}],
	tags: [
		'action',
		'adventure',
		'comedy',
		'horror',
		'mystery',
		'sci-fi',
		'thriller',
	],
}, {
	id: 'red-1',
	title: 'Red',
	image: 'red.jpg',
	homepage: 'http://redcomicbook.com?utm_source=indiecomicrack',
	description: `
	<p>Welcome to the action packed world of Red: A Cyberpunk Fairytale.</p>

	<p>Inspired by the classic Little Red Riding Hood tale, it tells the story of Red, a member of an elite private security firm. She lives, works, and fights to survive in a futuristic world that’s spiraling into a violent police state.</p>

	<p>When her grandmother becomes the victim of a bioterror attack, Red unravels a mystery involving the megacorporation known as Canis, the terrorist group known as The Lords of the Fourth Order, and the most clandestine levels of government.</p>

	<p>What Red discovers will change her life and the lives around her...forever.</p>
	`,
	samples: [{
		label: 'Page 1',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6ceb7e4b0ffd4557b88b8/1407635155342/1.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6ceb5e4b09aec917592a6/1407635134610/2.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6ceb7e4b065351cdf5bca/1407635140575/3.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6cebbe4b083c8a9bca88e/1407635132111/4.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 5',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6ceb8e4b065351cdf5bcc/1407635134441/5.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 6',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6ceb9e4b065351cdf5bce/1407635130487/6.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 7',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6cebbe4b065351cdf5bd4/1407635132129/7.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 8',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6cebee4b083c8a9bca893/1407635139574/8.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 9',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6cebee4b065351cdf5bd6/1407635135090/9.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 10',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6cebfe4b065351cdf5bd8/1407635136145/10.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 11',
		url: 'https://static1.squarespace.com/static/51f49267e4b086a9c87c5a4b/52008cefe4b00bacedd14e5a/53e6d112e4b02c4bc3ad61fc/1407635736081/11.jpg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Casey Bowker',
		credit: 'Creator',
		url: 'https://dontforgetatowel.com'
	}],
	tags: [
		'action',
		'sci-fi',
	],
}, {
	id: 'sovereign',
	title: 'Sovereign',
	image: 'sovereign.jpg',
	homepage: 'https://www.indiegogo.com/projects/sovereign?utm_source=indiecomicrack',
	description: `SOVEREIGN is an action-adventure epic that takes place in the year 2516 M.E. (Machine Era). A post-apocalyptic future where advanced sentient machines known as The Grey, have laid waste to most of humanity and are draining the planet of it’s Eon-Energy. But the Earth resurrects it’s champion, the destroyer and waste layer... Sovereign. But Sovereign is no hero. He is a force of nature and once the Grey is destroyed, will humans be next?`,
	samples: [{
		label: 'Cover 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540574503/bt5ddhjhaj3z3epcjzvj.jpg',
		type: 'IMAGE',
	}, {
		label: 'Cover 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540576162/x4eigpjvpo0qbypu1ull.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540576275/qiapna2rvfwpahtchhf2.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540576310/ywv9qgtpd1z3krqmsgka.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540576343/i4owzsfgxuyk4txgnylc.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540576416/ohnnxz3neydvjo9ngqwm.jpg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Andrew Huerta',
		credit: 'Writing/Penciling/Inking',
		url: 'https://www.artstation.com/andrewhuerta'
	}, {
		name: 'Joshua Perez',
		credit: 'Colors',
		url: 'https://www.artstation.com/andrewhuerta'
	}],
	tags: [
		'action',
		'anime',
		'manga',
		'post-apocalyptic',
		'sci-fi',
	],
}, {
	id: 'starside',
	title: 'Starside',
	image: 'starside.png',
	homepage: 'https://www.comixcentral.com/products/starside-1/?utm_source=indiecomicrack',
	description: `When an extraterrestrial fleet rips Jack away from his ordinary life on Earth, he is dropped into a war-torn universe where he discovers interplanetary slavers, a crusading army, and the origins of Humanity.`,
	samples: [{
		label: 'Page 1',
		type: 'IMAGE',
		url: '/assets/images/pages/starside/page1.png',
	}, {
		type: 'IMAGE',
		label: 'Page 2',
		url: '/assets/images/pages/starside/page2.png',
	}, {
		label: 'Page 3',
		type: 'IMAGE',
		url: '/assets/images/pages/starside/page3.png',
	}],
	credits: [],
	tags: [
		'action',
		'all ages',
		'fantasy',
		'sci-fi',
	],
}, {
	id: 'glaxial-freak-or-god',
	title: 'Glaxial: Freak or God',
	image: 'glaxial.png',
	homepage: 'https://www.indiegogo.com/projects/glaxial-freak-or-god-1?utm_source=indiecomicrack',
	description: `
	<p>
		Kralex is a 13 year old boy facinated by Time Circles. A huge city, the last city on Earth. Sadly, he is not born at the right place.  He is miles away from Time Circles. Kralex lives with a small weird community named The Gunners. They love guns.  They Really love guns.
	</p>
	<p>
		Kralex's cult community all committed suicide with guns (except Kralex).  Kralex decided to take all guns and try to reach Time Circles to find hope, love & a better life...and it's not really what he'll find
	</p>
	`,
	samples: [{
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,f_auto,w_620/v1536603382/ckjdbaejlor2pwgmqsvk.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,f_auto,w_620/v1536603382/mkbkv6vyth7vrf3ir7ps.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,f_auto,w_620/v1536603382/avj5bozxb1w9xu1ptmuh.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,f_auto,w_620/v1536603382/bfbf0rz7hjsewoyv8std.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 5',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,f_auto,w_620/v1536603382/cf6saljnvfzpijepuxqn.jpg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Kevin Roditeli',
		credit: 'Writer',
		url: 'https://www.kevinroditeli.com/',
	}, {
		name: 'Rob Cannon',
		credit: 'Artist',
		url: '',
	}],
	tags: [
		'sci-fi',
		'post-apocalyptic',
	],
}, {
	id: 'conflict',
	title: 'CONFLICT: Claws Out',
	image: 'conflict.png',
	homepage: 'https://www.indiegogo.com/projects/conflict-claws-out/?utm_source=indiecomicrack',
	description: `
	<p>
		Follow the stories of some very different people who are trying to survive the world, after a prehistoric virus reemerges and changes their lives, forever. The H1NV virus causes a variety of issues, but the long and the short of it is simple: IF you survive, men tend to display traits similar to what we think of as werewolves, while many women fall more into the category of what we think of as vampires. As you can imagine, humans get caught in the middle and it quickly becomes a race to survive, in CONFLICT: Claws Out.
	</p>
	`,
	samples: [{
		label: 'Page 1 Complete',
		url: '/assets/images/pages/conflict/page-1-complete.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1541402362/youcsrm9vmslh6juiubx.png',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540007024/r5yzxitjre7m3soefbqf.png',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540007205/nidl709bjvlbre8rxcgd.png',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540007251/mexrzbqxu9wsm9yaa2ye.png',
		type: 'IMAGE',
	}, {
		label: 'Page 5',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540007310/lq6yckk36me0rma43wyb.png',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'James Dean Anderson',
		credit: 'Writer',
		url: 'https://twitter.com/JamesDeanAnder1',
	}],
	tags: [
		'action',
		'adventure',
		'sci-fi',
		'fantasy',
		'horror',
		'comedy',
		'post-apocalyptic',
	],
},
theTommyGunDolls,
detectiveDeadCrimsonMoon,
whatIsMyrromGalaxyOath,
mashboneAndGrifty,
lemonPepperHuggz,
electromagnate,
knights700,
theAbductables,
deathSworn,
];
listingsData.reverse();

export { listingsData };