import { Listing } from "../models";

export
const lemonPepperHuggz: Listing = {
	id: 'lemon-pepper-huggz',
	title: 'Lemon, Pepper, Huggz',
	image: 'lemon-pepper-huggz.png',
	homepage: 'https://5meatscomics.bigcartel.com/product/lemon-pepper-huggz-comics?utm_source=indiecomicrack',
	description: `
		<p>
			Lemon, Pepper, Huggz is about three Elementary-age kids and the crazy after-school adventures they go on within their neighborhood.
		</p>
		<p>
			The humor and attitude of 5 MEATS COMICS, but for KIDS!
		</p>
		<p>
			ISSUE 1- Cookietown
		</p> 
		<p>
			When Lemon finds out the biggest Roller Coaster in New Brownsville will close soon, she, along with Pepper and Huggz, opens a Lemonade stand to raise ticket money. Unfortunately, the Cookie Scouts lay claim to the sidewalk, and they aren’t in the sharing mood!
		</p>
	`,
	samples: [],
	credits: [{
		name: 'Oscar Garza',
		credit: 'Art and Stroy',
		url: 'https://twitter.com/5meats',
	}, {
		name: 'Rolando Esquivel',
		credit: 'Edit and Story',
		url: 'https://twitter.com/Rolo5Meats/',
	}],
	tags: [
		'comedy',
		'adventure',
		'family',
	],
}