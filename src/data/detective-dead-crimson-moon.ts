import { Listing } from "../models";

export
const detectiveDeadCrimsonMoon: Listing = {
	id: 'detective-dead-crimson-moon',
	title: 'Detective Dead: Crimson Moon',
	image: 'detective-dead-crimson-moon.jpg',
	homepage: 'https://www.indiegogo.com/projects/detective-dead-crimson-moon/x/19378705/?utm_source=indiecomicrack',
	description: `
		<p>
			After a deadly raid against some of the most notorious gangsters of 1932, Joel Anthony Scorn, a Rusty Belt City Detective, heads home only to discover the lifeless bodies of his wife and daughter. Joel now finds himself in the modern day chasing down the man responsible, as well as every gangster who died at the raid that night. Unfortunately, like Joel, these thugs gained unique abilities of their own.
		</p>
		<p>
			Come join Detective Dead on this action packed 104-plus page colored graphic novel!
		</p>
	`,
	samples: [{
		label: 'Variant Cover',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1539713527/o5gm1r06qhe2lonyemhe.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 1',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1536685066/vsuvxflk8f5fbkmwrvdy.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 2',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540488991/awevzhpd1sc4dvfj16dg.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 3',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540489044/xoswsojrxfgmzu49hcd5.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 4',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540673610/liln2kywoajwu6kkboex.jpg',
		type: 'IMAGE',
	}, {
		label: 'Page 5',
		url: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1540673628/gfian1dktbv4jzufxnyf.jpg',
		type: 'IMAGE',
	}],
	credits: [{
		name: 'Antonio Malpica',
		credit: 'Creator',
		url: 'https://twitter.com/ScornComiX',
	}],
	tags: [
		'crime',
		'noir',
		'sci-fi',
	],
}