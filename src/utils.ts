export
function shuffleArray<T>(list: Array<T>) {
	return list
		.map((a) => ({sort: Math.random(), value: a}))
		.sort((a, b) => a.sort - b.sort)
		.map((a) => a.value);
}