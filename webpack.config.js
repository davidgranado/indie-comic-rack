const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
	entry: './src/index.tsx',
	output: {
		path: path.resolve('./public'),
		filename: 'index.js'
	},
	resolve: {
		// Add `.ts` and `.tsx` as a resolvable extension.
		extensions: ['.ts', '.tsx', '.js']
	},
	module: {
		rules: [
			// all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: {
					loader: 'ts-loader',
				}
			},
		],
	},
	plugins: [
		new CopyWebpackPlugin([{
			//Note:- No wildcard is specified hence will copy all files and folders
			from: 'static', //Will resolve to RepoDir/src/assets 
			// to: '.' //Copies all files from above dest to dist/assets
			
		}]),
	],
};
